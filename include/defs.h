#ifndef DEFS_H
#define DEFS_H

#define SCREEN_WIDTH			1920
#define SCREEN_HEIGHT			1080

#define PLAYER_SPEED			8
#define PLAYER_BULLET_SPEED		16

#define MAX_KEYBOARD_KEYS		350

#define SIDE_PLAYER				0
#define SIDE_ENEMY				1

#define FPS						60

#define ENEMY_BULLET_SPEED		8

#define MAX_STARS				500

#define MAX_SND_CHANNELS 		8

#define MAX_LINE_LENGTH			1024

#define NUM_HIGHSCORES          10

#define STRNCPY(dest, src, n) strncpy(dest, src, n); dest[n - 1] = '\0'

#define MAX_NAME_LENGTH         32

#define MAX_SCORE_NAME_LENGTH   16

enum
{
	CH_ANY = -1,
	CH_PLAYER,
	CH_ENEMY_FIRE,
	CH_POINTS
};

enum
{
	SND_PLAYER_FIRE,
	SND_ENEMY_FIRE,
	SND_PLAYER_DIE,
	SND_ENEMY_DIE,
	SND_POINTS,
	SND_MAX,
	SND_GAME_START,
	SND_GAME_OVER
};

enum
{
    TEXT_LEFT,
    TEXT_CENTER,
    TEXT_RIGHT
};

#endif
