#ifndef DEBRIS_H
#define DEBRIS_H

#include <structs.h>

void doDebris();

void addDebris(Entity *e);

void drawDebris();

#endif
