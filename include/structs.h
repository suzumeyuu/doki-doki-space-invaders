#ifndef STRUCTS_H
#define STRUCTS_H

#include <defs.h>
#include <SDL.h>

struct Delegate
{
	void (*logic)();
	void (*draw)();
};

struct Texture
{
    char name[MAX_NAME_LENGTH];
    SDL_Texture *texture;
    Texture *next;
};

struct App
{
    SDL_Renderer *renderer;
    SDL_Window *window;
    Delegate delegate;
    int keyboard[MAX_KEYBOARD_KEYS];
    Texture textureHead, *textureTail;
    char inputText[MAX_LINE_LENGTH];
};

struct Entity
{
    float x;
    float y;
    int w;
    int h;
    float dx;
    float dy;
    int health;
    int reload;
    SDL_Texture *texture;
    Entity *next;
    int side;
    Uint32 lifeStartTime;
};

struct Explosion
{
	float x;
	float y;
	float dx;
	float dy;
	int r, g, b, a;
	Explosion *next;
};

struct Debris
{
	float x;
	float y;
	float dx;
	float dy;
	SDL_Rect rect;
	SDL_Texture *texture;
	int life;
	Debris *next;
};

struct Stage
{
	Entity fighterHead, *fighterTail;
	Entity bulletHead, *bulletTail;
	Entity pointsHead, *pointsTail;
	Explosion explosionHead, *explosionTail;
	Debris debrisHead, *debrisTail;
	int score;
};

struct Star
{
	int x;
	int y;
	int speed;
};

struct Highscore
{
    int recent;
    int score;
    char name[MAX_SCORE_NAME_LENGTH];
};

struct Highscores
{
    Highscore highscore[NUM_HIGHSCORES];
};

extern SDL_Texture *bulletTexture;
extern SDL_Texture *enemyTexture;
extern SDL_Texture *playerTexture;
extern SDL_Texture *enemyBulletTexture;
extern SDL_Texture *background;
extern SDL_Texture *explosionTexture;
extern SDL_Texture *pointsTexture;
extern SDL_Texture *bonusPointsTexture;
extern SDL_Texture *enemyKilledTexture;
extern SDL_Texture *fog;

extern struct App app;
extern struct Entity *player;
extern struct Stage stage;
extern struct Explosion explosion;
extern struct Debris debris;
extern struct Star stars[MAX_STARS];

extern int pointsCounter;
extern int pointsPods;
extern int textWidth;
extern int textHeight;

extern bool bonusPoints;

extern Uint32 pointCatchTime;

#endif
