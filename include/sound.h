#ifndef SOUND_H
#define SOUND_H

void initSounds();

static void loadSounds();

void loadMusic(char *filename);

void playMusic(int loop);

void playSound(int id, int channel);

void deinitSounds();

void stopMusic();

#endif
