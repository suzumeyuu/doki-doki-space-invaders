#ifndef TEXT_H
#define TEXT_H

#include <SDL.h>

#define GLYPH_WIDTH		100;
#define GLYPH_HEIGHT	100;

void initText();

void drawText(int x, int y, Uint8 r, Uint8 g, Uint8 b, int align, char *format, ...);

void drawHud();

void deleteText();

void deinitText();

extern int highscore;

#endif
