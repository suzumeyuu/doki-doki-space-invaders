#ifndef DRAW_H
#define DRAW_H

#include <SDL.h>

void prepareScene(int R, int G, int B, int A);

void presentScene();

SDL_Texture *loadTexture(char *filename);

static SDL_Texture *getTexture(char *name);

static void addTextureToCache(char *name, SDL_Texture *sdlTexture);

void blit(SDL_Texture *texture, int x, int y);

void bullets(SDL_Texture *texture, int x, int y);

void blitRect(SDL_Texture *texture, SDL_Rect *src, int x, int y);

extern int frame;

#endif
