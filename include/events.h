#ifndef EVENTS_H
#define EVENTS_H

#include <structs.h>

void doFighters();

void doBullets();

int bulletHitFighter(Entity *b);

void fireBullet();

void doPlayer();

void doEnemies();

void fireEnemyBullet(Entity *e);

void doPointsPods();

void addPointsPod(int x, int y);

int countPoints(int startPoints, int pointsStep, Entity *e);

#endif
