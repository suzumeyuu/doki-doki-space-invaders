#ifndef STAGE_H
#define STAGE_H

static void initPlayer();

void initStage();

void deinitStage();

static void logic();

static void drawBullets();

static void draw();

void capFrameRate(long *then, float *remainder);

static void spawnEnemies();

static void drawFighters();

static void drawPointsPods();

static void resetStage();

static void clipPlayer();

static void doBackground();

void drawBackground();

static void doFog();

static void drawFog();

void initBackground();

#endif
