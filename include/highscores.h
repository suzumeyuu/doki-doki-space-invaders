#ifndef HIGHSCORES_H
#define HIGHSCORES_H

void initHighscoreTable();

void initHighscores();

static void doNameInput();

static void logic();

static void drawNameInput();

static void draw();

static void drawHighscores();

void addHighscore(int score);

static int highscoreComparator(const void *a, const void *b);

#endif
