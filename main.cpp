#include <SDL.h>
#include <structs.h>
#include <init.h>
#include <input.h>
#include <draw.h>
#include <cstdlib>
#include <ctime>
#include <defs.h>
#include <stage.h>
#include <sound.h>
#include <cstring>
#include <cstdio>
#include <SDL_mixer.h>
#include <text.h>
#include <SDL_ttf.h>
#include <highscores.h>

struct Entity *player = NULL;
struct App app;
struct Stage stage;
struct Explosion explosion;
struct Debris debris;
struct Star stars[MAX_STARS];

int frame = 0;

void cleanup()
{	
	return;
}

int main()
{	
	memset(&app, 0, sizeof(App));
	
    initSDL();

    atexit(cleanup);
	
	initStage();
	
    initSounds();
	
	TTF_Init();
	
	initText();

    initGame();
    
	long then = SDL_GetTicks();
	
	float remainder = 0;
	
    srand(time(NULL));

    playSound(SND_GAME_START, CH_PLAYER);

    while(1)
    {
    	doInput();
                
        app.delegate.logic();
                
        app.delegate.draw();
                
        ++frame;
                	
        presentScene();
                
        capFrameRate(&then, &remainder);
    }

    deinitSounds();

    deinitStage();

	deinitText();

	TTF_Quit();

    deinitSDL();

    SDL_Quit();

    return 0;
}
