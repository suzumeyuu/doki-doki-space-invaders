OBJECTS = main.o \
          src/draw.o \
          src/init.o \
          src/input.o \
          src/stage.o \
          src/util.o \
          src/starfield.o \
          src/explosion.o \
          src/debris.o \
          src/events.o \
          src/sound.o \
          src/text.o \
          src/highscores.o


CPPFLAGS = `pkg-config --cflags sdl2` -fsanitize=address -g -Iinclude/
LDFLAGS = -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf -lasan


zsrr: $(OBJECTS)
	g++ $(OBJECTS) $(CPPFLAGS) $(LDFLAGS) -o ddsi
	
all: ddsi

clean:
	rm $(OBJECTS) ddsi
