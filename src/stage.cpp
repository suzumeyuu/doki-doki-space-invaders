#include <structs.h>
#include <draw.h>
#include <stage.h>
#include <util.h>
#include <starfield.h>
#include <debris.h>
#include <explosion.h>
#include <events.h>
#include <SDL.h>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <sound.h>
#include <text.h>
#include <highscores.h>

SDL_Texture *enemyTexture = NULL;
SDL_Texture *playerTexture = NULL;
SDL_Texture *background = NULL;
SDL_Texture *explosionTexture = NULL;
SDL_Texture *pointsTexture = NULL;
SDL_Texture *bonusPointsTexture = NULL;
SDL_Texture *enemyKilledTexture = NULL;
SDL_Texture *fog = NULL;

int enemySpawnTimer;
int stageResetTimer;

int fogPosition = 0;
int backgroundPosition = 0; //FIXME: Ta zmienna jest do niczego nie potrzebna, gdyż obraz się nie przewija

static void initPlayer()
{
	player = new Entity;

	memset(player, 0, sizeof(Entity));
	
	stage.fighterTail->next = player;
	stage.fighterTail = player;
	
	player->side = SIDE_PLAYER;
	player->x = 100;
	player->y = 100;
	player->health = 1;
	
	player->texture = playerTexture;
	
	SDL_QueryTexture(player->texture, NULL, NULL, &player->w, &player->h);
}

void deinitStage()
{
    SDL_DestroyTexture(enemyTexture);
    SDL_DestroyTexture(playerTexture);
    SDL_DestroyTexture(background);
    SDL_DestroyTexture(explosionTexture);
    SDL_DestroyTexture(pointsTexture);
    SDL_DestroyTexture(bonusPointsTexture);
    SDL_DestroyTexture(enemyKilledTexture);
    SDL_DestroyTexture(fog);
}

void initBackground()
{	
	if(background == NULL)
	    background = loadTexture("textures/background.png");
}

void initStage()
{
	app.delegate.logic = logic;
	app.delegate.draw = draw;
	
	memset(&stage, 0, sizeof(Stage));
	
	stage.fighterTail = &stage.fighterHead;
	stage.bulletTail = &stage.bulletHead;
	
	initPlayer();
	
    if(bulletTexture == NULL)
	    bulletTexture = loadTexture("textures/playerBullet.png");
	
    if(enemyTexture == NULL)
        enemyTexture = loadTexture("textures/enemy.png");
	
	enemySpawnTimer = 0;
	
    if(enemyBulletTexture == NULL)	
        enemyBulletTexture = loadTexture("textures/enemyBullet.png");

    if(enemyKilledTexture == NULL)
        enemyKilledTexture = loadTexture("textures/enemyKilled.png");
	
    if(playerTexture == NULL)
        playerTexture = loadTexture("textures/player.png");
	
    if(pointsTexture == NULL)
        pointsTexture = loadTexture("textures/points.png");

    if(bonusPointsTexture == NULL)
        bonusPointsTexture = loadTexture("textures/bonusPoints.png");

	stage.explosionTail = &stage.explosionHead;
	stage.debrisTail = &stage.debrisHead;
	
	resetStage();
	
    if(background == NULL)
	    background = loadTexture("textures/background.png");
	
    if(explosionTexture == NULL)
        explosionTexture = loadTexture("textures/explosion.png");

    if(fog == NULL)
	    fog = loadTexture("textures/fog.png");
}

static void resetStage()
{
	Entity *e;
	Explosion *ex;
	Debris *d;
	
	while(stage.fighterHead.next)
	{
		e = stage.fighterHead.next;
		
		stage.fighterHead.next = e->next;
		
		delete e;
	}
	
	while(stage.bulletHead.next)
	{
		e = stage.bulletHead.next;
		
		stage.bulletHead.next = e->next;
		
		delete e;
	}
	
	while(stage.pointsHead.next)
	{
		e = stage.pointsHead.next;
		
		stage.pointsHead.next = e->next;
		
		delete e;
	}

	while(stage.explosionHead.next)
	{
		ex = stage.explosionHead.next;
		
		stage.explosionHead.next = ex->next;
		
		delete ex;
	}
	
	while(stage.debrisHead.next)
	{
		d = stage.debrisHead.next;
		
		stage.debrisHead.next = d->next;
		
		delete d;
	}
	
	memset(&stage, 0, sizeof(Stage));
	
	stage.fighterTail = &stage.fighterHead;
	stage.bulletTail = &stage.bulletHead;
    	stage.pointsTail = &stage.pointsHead;
	
	stage.explosionTail = &stage.explosionHead;
	stage.debrisTail = &stage.debrisHead;
	
	initPlayer();
	initStarfield();
	
	enemySpawnTimer = 0;
	
	stageResetTimer = FPS * 3;

    stage.score = 0;
	
	pointsCounter = 0;
	pointsPods = 0;
	
	deleteText();
	
	stopMusic();
	
	playSound(SND_GAME_START, CH_PLAYER);
}

static void logic()
{	
	doBackground();
	
	doStarfield();
	
	doPlayer();
		
	doFighters();
	
	doBullets();
	
	spawnEnemies();

	doEnemies();
	
    	doPointsPods();

	clipPlayer();
	
	doExplosions();
	
	doDebris();

        doFog();
		
	if(player == NULL && --stageResetTimer <= 0)
	{
        initHighscores();
	    
	    addHighscore(stage.score);
	    
	    playMusic(1);    
    }
}

static void doFog()
{
	if(--fogPosition < -SCREEN_WIDTH)
		fogPosition = 0;
}

static void doBackground() //FIXME: Sprawdzić czy jest to potrzebne w grze, jeśli to służy do przewijania obrazu to do wywalenia gdyż grafika w tle jest nieruchoma - jedynie mgła się rusza
{
	if(--backgroundPosition < -SCREEN_WIDTH)
		backgroundPosition = 0;
}

static void spawnEnemies()
{
	Entity *enemy = new Entity;
	
	static int dx = -1;
	
	if(--enemySpawnTimer <= 0)
	{
		memset(enemy, 0, sizeof(Entity));
		
		stage.fighterTail->next = enemy;
		stage.fighterTail = enemy;
		
		enemy->side = SIDE_ENEMY;
		enemy->x = SCREEN_WIDTH;
		enemy->y = rand() % (SCREEN_HEIGHT - 384);
		enemy->health = 1;
		enemy->texture = enemyTexture;
		
		SDL_QueryTexture(enemy->texture, NULL, NULL, &enemy->w, &enemy->h);
		
		enemy->dx = -(2 + (rand() % 7));
		
		while(enemy->dx == dx)
			enemy->dx = -(2 + (rand() % 7));
		
		dx = enemy->dx;
		
		enemy->reload = FPS * (1 + (rand() % 3));
		
		enemySpawnTimer = 30 + (rand() % 61);
	}
}

static void clipPlayer()
{
	if(player != NULL)
	{
		if(player->x < 0)
			player->x = 0;
			
		if(player->y < 0)
			player->y = 0;
			
		if(player->x > SCREEN_WIDTH / 2)
			player->x = SCREEN_WIDTH / 2;
			
		if(player->y > SCREEN_HEIGHT - player->h)
			player->y = SCREEN_HEIGHT - player->h;
	}
}

static void drawFighters()
{
	Entity *e;
	
	for(e = stage.fighterHead.next; e != NULL; e = e->next)
		blit(e->texture, e->x, e->y);
}

static void drawPointsPods()
{
	Entity *e;
	
	for(e = stage.pointsHead.next; e != NULL; e = e->next)
		bullets(e->texture, e->x, e->y);
}

static void drawBullets()
{
	Entity *b;
	
	for(b = stage.bulletHead.next; b != NULL; b = b->next)
		bullets(b->texture, b->x, b->y);
}

void drawBackground()
{
	SDL_Rect dest;
	
	dest.x = 0;
	dest.y = 0;
	dest.w = SCREEN_WIDTH;
	dest.h = SCREEN_HEIGHT;
		
	SDL_RenderCopy(app.renderer, background, NULL, &dest);
}

static void drawFog()
{
    SDL_Rect dest;

    for(int x = fogPosition; x < SCREEN_WIDTH; x += SCREEN_WIDTH)
    {
        dest.x = x;
	    dest.y = 0;
	    dest.w = SCREEN_WIDTH;
	    dest.h = SCREEN_HEIGHT;

        SDL_RenderCopy(app.renderer, fog, NULL, &dest);
    }
}

static void draw()
{
	drawBackground();
	
	drawStarfield();
	
	drawBullets();
	
	drawFighters();
	
	drawHud();

    drawFog();

    drawPointsPods();
	
	drawDebris();
	
	drawExplosions();
}

void capFrameRate(long *then, float *remainder)
{
	long wait, frameTime;
	
	wait = 16 + *remainder;
	
	*remainder -= (int)*remainder;
	
	frameTime = SDL_GetTicks() - *then;
	
	wait -= frameTime;
	
	if(wait < 1)
		wait = 1;
	
	SDL_Delay(wait);
	
	*remainder += 0.667;
	
	*then = SDL_GetTicks();
}
