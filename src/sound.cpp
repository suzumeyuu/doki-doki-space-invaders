#include <sound.h>
#include <defs.h>
#include <cstdlib>
#include <SDL.h>
#include <SDL_mixer.h>

Mix_Music *music = NULL;
Mix_Chunk *sounds[MAX_SND_CHANNELS];

void initSounds()
{
	memset(sounds, 0, sizeof(Mix_Chunk*) * SND_MAX);
	
	music = NULL;
	
	loadSounds();
}

static void loadSounds()
{
	//sounds[SND_PLAYER_FIRE] = Mix_LoadWAV("sound/[nazwa pliku].wav");
	
	//sounds[SND_ENEMY_FIRE] = Mix_LoadWAV("sound/[nazwa pliku].wav");
		
	//sounds[SND_PLAYER_DIE] = Mix_LoadWAV("sound/[nazwa pliku].wav");

	sounds[SND_ENEMY_DIE] = Mix_LoadWAV("sound/enemy_death.wav");
	
	sounds[SND_POINTS] = Mix_LoadWAV("sound/points.wav");
	
	sounds[SND_GAME_START] = Mix_LoadWAV("sound/game_start.wav");
	
	sounds[SND_GAME_OVER] = Mix_LoadWAV("sound/game_over.wav");
}

void loadMusic(char *filename)
{
	if(music != NULL)
	{
		Mix_HaltMusic();
		Mix_FreeMusic(music);
		music = NULL;
	}
	
	music = Mix_LoadMUS(filename);
}

void playMusic(int loop)
{
	Mix_PlayMusic(music, (loop) ? -1 : 0);
}

void stopMusic()
{
    Mix_HaltMusic();
}

void playSound(int id, int channel)
{
	Mix_PlayChannel(channel, sounds[id], 0);
}

void deinitSounds()
{
    Mix_FreeChunk(sounds[SND_ENEMY_FIRE]);
    Mix_FreeChunk(sounds[SND_ENEMY_DIE]);
    Mix_FreeChunk(sounds[SND_POINTS]);
    Mix_FreeChunk(sounds[SND_GAME_START]);
    Mix_FreeChunk(sounds[SND_GAME_OVER]);
    Mix_HaltMusic();
	Mix_FreeMusic(music);
    Mix_Quit();
}
