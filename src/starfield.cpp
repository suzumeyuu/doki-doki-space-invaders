#include <starfield.h>
#include <defs.h>
#include <structs.h>
#include <SDL.h>
#include <cstdlib>

void initStarfield()
{
	for(int i = 0; i < MAX_STARS; i++)
	{
		stars[i].x = rand() % SCREEN_WIDTH;
		stars[i].y = rand() % SCREEN_HEIGHT;
		stars[i].speed = 1 + rand() % 8;
	}
}

void doStarfield()
{
	for(int i = 0; i < MAX_STARS; i++)
	{
		stars[i].x -= stars[i].speed;
		
		if(stars[i].x < 0)
			stars[i].x = SCREEN_WIDTH + stars[i].x;
	}
}

void drawStarfield()
{
	for(int i = 0; i< MAX_STARS; i++)
	{
		int c = 32 * stars[i].speed;
		
		SDL_SetRenderDrawColor(app.renderer, c, c, c, 255);
		
		SDL_RenderDrawLine(app.renderer, stars[i].x, stars[i].y, stars[i].x + 3, stars[i].y);
	}
}
