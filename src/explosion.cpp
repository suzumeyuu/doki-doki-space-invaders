#include <explosion.h>
#include <structs.h>
#include <draw.h>
#include <SDL.h>
#include <cstdlib>

void doExplosions()
{
	Explosion *e, *prev;
	
	prev = &stage.explosionHead;
	
	for(e = stage.explosionHead.next; e != NULL; e = e->next)
	{
		e->x += e->dx;
		e->y += e->dy;
		
		if(--e->a <= 0)
		{
			if(e == stage.explosionTail)
				stage.explosionTail = prev;
				
			prev->next = e->next;
			
			delete e;
			
			e = prev;
		}
		
		prev = e;
	}
}

void addExplosions(int x, int y, int num)
{
	Explosion *e = NULL;
	
	for(int i = 0; i < num; i++)
	{
		e = new Explosion;
		memset(e, 0, sizeof(Explosion));
		
		stage.explosionTail->next = e;
		stage.explosionTail = e;
		
		e->x = x + (rand() % 32) - (rand() % 32);
		e->y = y + (rand() % 32) - (rand() % 32);
		e->dx = (rand() % 10) - (rand() % 10);
		e->dy = (rand() % 10) - (rand() % 10);
		
		e->dx /= 10;
		e->dy /= 10;
		
		switch(rand() % 4)
		{
			case 0:
				e->r = 255;
				break;
			
			case 1:
				e->r = 255;
				e->g = 128;
				break;
			
			case 2:
				e->r = 255;
				e->g = 255;
				break;
				
			default:
				e->r = 255;
				e->g = 255;
				e->b = 255;
				break;
		}
		
		e->a = rand() % FPS * 3;
	}
}

void drawExplosions()
{
	Explosion *e;
	
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_ADD);
	SDL_SetTextureBlendMode(explosionTexture, SDL_BLENDMODE_ADD);
	
	for(e = stage.explosionHead.next; e != NULL; e = e->next)
	{
		SDL_SetTextureColorMod(explosionTexture, e->r, e->g, e->b);
		SDL_SetTextureAlphaMod(explosionTexture, e->a);
		
		blit(explosionTexture, e->x, e->y);
	}
	
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);
}
