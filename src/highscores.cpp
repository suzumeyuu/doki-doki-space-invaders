#include <highscores.h>
#include <defs.h>
#include <structs.h>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <text.h>
#include <stage.h>
#include <string>
#include <starfield.h>

Highscores *highscores = new Highscores;
Highscore *newHighscore = new Highscore;

//int cursorBlink = 0;

void initHighscoreTable()
{
    memset(highscores, 0, sizeof(Highscores));
    
    for(int i = 0; i < NUM_HIGHSCORES; i++)
    {
        highscores->highscore[i].score = NUM_HIGHSCORES - i;
        STRNCPY(highscores->highscore[i].name, "ANONYMOUS", MAX_SCORE_NAME_LENGTH);
    }
    
    newHighscore = NULL;
    
//    cursorBlink = 0;
}

void initHighscores()
{
    app.delegate.logic = logic;
    app.delegate.draw = draw;
    
    memset(app.keyboard, 0, sizeof(int) * MAX_KEYBOARD_KEYS);
}

static void doNameInput()
{
    int n = strlen(newHighscore->name);
    
//TODO Spróbować obsłużyć wypadek gdy gracz wstawi same spacje, bo tego kod nie wychwyci i nie zastąpi 'ANONYMOUS'
    
    for(int i = 0; i < strlen(app.inputText); i++)
    {
        if(n < MAX_SCORE_NAME_LENGTH - 1)
            newHighscore->name[n++] = app.inputText[i];
    }
    
    if(n > 0 && app.keyboard[SDL_SCANCODE_BACKSPACE])
    {
        newHighscore->name[--n] = '\0';
        
        app.keyboard[SDL_SCANCODE_BACKSPACE] = 0;
    }
    
    if(app.keyboard[SDL_SCANCODE_RETURN])
    {
        if(strlen(newHighscore->name) == 0)
            STRNCPY(newHighscore->name, "ANONYMOUS", MAX_SCORE_NAME_LENGTH);
            
        newHighscore = NULL;
    }
}

static void logic()
{
    //doBackground();
    
    doStarfield();
    
    if(newHighscore != NULL)
        doNameInput();
        
    else
        if(app.keyboard[SDL_SCANCODE_LCTRL])
            initStage();
            
//    if(++cursorBlink >= FPS)
//        cursorBlink = 0;
}

static void drawNameInput()
{
    SDL_Rect r;
    
    drawText(SCREEN_WIDTH / 2, 70, 120, 204, 250, TEXT_CENTER, "CONGRATULATIONS, YOU'VE GAINED A HIGHSCORE!");
    
    drawText(SCREEN_WIDTH / 2, 120, 120, 204, 250, TEXT_CENTER, "ENTER YOUR NAME BELOW:");
    
    drawText(SCREEN_WIDTH / 2, 170, 255, 255, 255, TEXT_CENTER, newHighscore->name);
    
//    if(cursorBlink < FPS / 2)
//    {
        r.x = ((SCREEN_WIDTH / 2) + (strlen(newHighscore->name) * textWidth) / 2) + 5;
        r.y = 250;
        r.w = textWidth;
        r.h = textHeight;
        
        //SDL_SetRenderDrawColor(app.renderer, 0, 255, 0, 255);
        //SDL_RenderFillRect(app.renderer, &r);
//    }
    
    drawText(SCREEN_WIDTH / 2, 625, 120, 204, 250, TEXT_CENTER, "PRESS RETURN WHEN FINISHED!");
}

static void draw()
{
    drawBackground();
    drawStarfield();
    
    if(newHighscore != NULL)
        drawNameInput();
    
    else
        drawHighscores();
}

static void drawHighscores()
{
    int y = 200; //zamiast 200 było 150
    
    drawText(SCREEN_WIDTH / 2, 100, 120, 204, 250, TEXT_CENTER, "HIGHSCORES"); //zamiast 100 było 105
    
    int r = 255;
    int g = 255;
    int b = 255;
    
    for(int i = 0; i < NUM_HIGHSCORES; i++)
    {
        if(highscores->highscore[i].recent)
            b = 0;
        
        else if(highscores->highscore[i].score == 666) //satan score
        {
            g = 0;
            b = 0;
        }
           
        drawText(SCREEN_WIDTH / 2, y, 255, 255, 255, TEXT_CENTER, "#%02d. %-15s  ......  %03d", (i+1), highscores->highscore[i].name, highscores->highscore[i].score);
            
        y += 50;
    }
    
    drawText((SCREEN_WIDTH / 2), (y + 50), 120, 204, 250, TEXT_CENTER, "PRESS FIRE TO PLAY!"); //y + 50 zamiast y, aby odległość od tabeli wyników zmieniała się w zależności od ilości wierszy z wynikami
}

void addHighscore(int score)
{
    Highscore newHighscores[NUM_HIGHSCORES + 1];
    
    for(int i = 0; i < NUM_HIGHSCORES; i++)
    {
        newHighscores[i] = highscores->highscore[i];
        newHighscores[i].recent = 0;
    }
    
    newHighscores[NUM_HIGHSCORES].score = score;
    newHighscores[NUM_HIGHSCORES].recent = 1;
    
    qsort(newHighscores, NUM_HIGHSCORES + 1, sizeof(Highscore), highscoreComparator);
    
    for(int i = 0; i < NUM_HIGHSCORES; i++)
        highscores->highscore[i] = newHighscores[i];
    
    newHighscore = NULL;
    
    for(int i = 0; i < NUM_HIGHSCORES; i++)
    {
        highscores->highscore[i] = newHighscores[i];
        
        if(highscores->highscore[i].recent)
        {
            newHighscore = &highscores->highscore[i];
            memset(newHighscore->name, '\0', sizeof(newHighscore->name));
        }
    }
}

static int highscoreComparator(const void *a, const void *b)
{
    Highscore *h1 = ((Highscore*)a);
    Highscore *h2 = ((Highscore*)b);
    
    return h2->score - h1->score;
}
