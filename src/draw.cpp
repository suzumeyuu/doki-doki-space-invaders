#include <draw.h>
#include <structs.h>
#include <SDL.h>
#include <SDL_image.h>
#include <cstring>
#include <cstdio>

void prepareScene(int R, int G, int B, int A)
{
	SDL_SetRenderDrawColor(app.renderer, R,G,B,A);
	SDL_RenderClear(app.renderer);
}

void presentScene()
{
	SDL_RenderPresent(app.renderer);
}

SDL_Texture *loadTexture(char *filename)
{
    SDL_Texture *texture;

    texture = getTexture(filename);
    
    if(texture == NULL)
    {
        SDL_LogMessage(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_INFO, "Loading %s", filename);

        texture = IMG_LoadTexture(app.renderer, filename);
        
        addTextureToCache(filename, texture);
    }
    
    return texture;
}

static SDL_Texture *getTexture(char *name)
{   
    Texture *t;
    
    for(t = app.textureHead.next; t != NULL; t = t->next)
        if(strcmp(t->name, name) == 0)
            return t->texture;
            
    return NULL;
}

static void addTextureToCache(char *name, SDL_Texture *sdlTexture)
{
    Texture *texture = new Texture;
    
    memset(texture, 0, sizeof(Texture));
    
    if(app.textureTail == NULL)
        app.textureTail = texture;
        
    else
    {
        app.textureTail->next = texture;
        app.textureTail = texture;
    }
    
    STRNCPY(texture->name, name, MAX_NAME_LENGTH);
    texture->texture = sdlTexture;
}

void blit(SDL_Texture *texture, int x, int y)
{
    SDL_Rect dest;

    dest.x = x;
    dest.y = y;
    
    SDL_QueryTexture(texture, NULL, NULL, &dest.w, &dest.h);
    
    SDL_RenderCopy(app.renderer, texture, NULL, &dest);
}

void bullets(SDL_Texture *texture, int x, int y)
{
	SDL_Rect SpriteClips[4];
	
	SpriteClips[ 0 ].x =   0;
	SpriteClips[ 0 ].y =   0;
	SpriteClips[ 0 ].w =  50;
	SpriteClips[ 0 ].h =  50;

	SpriteClips[ 1 ].x =  50;
	SpriteClips[ 1 ].y =   0;
	SpriteClips[ 1 ].w =  50;
	SpriteClips[ 1 ].h =  50;
		
	SpriteClips[ 2 ].x = 100;
	SpriteClips[ 2 ].y =   0;
	SpriteClips[ 2 ].w =  50;
	SpriteClips[ 2 ].h =  50;

	SpriteClips[ 3 ].x = 150;
	SpriteClips[ 3 ].y =   0;
	SpriteClips[ 3 ].w =  50;
	SpriteClips[ 3 ].h =  50;
                
    SDL_Rect * currentClip = &SpriteClips[frame / 8];
   	
   	SDL_Rect clip;
   	
   	clip.x = x;
   	clip.y = y;
   	clip.w = 50;
   	clip.h = 50; 
   	           
    SDL_RenderCopy(app.renderer, texture, currentClip, &clip);
                
    if(frame / 8 >= 4)
    	frame = 0;
}

void blitRect(SDL_Texture *texture, SDL_Rect *src, int x, int y)
{
	SDL_Rect dest;
	dest.x = x;
	dest.y = y;
	dest.w = src->w;
	dest.h = src->h;
	
	SDL_RenderCopy(app.renderer, texture, src, &dest);
}
