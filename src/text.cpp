#include <text.h>
#include <SDL.h>
#include <SDL_ttf.h>
#include <cstdarg>
#include <cstdio>
#include <cstring>
#include <structs.h>
#include <draw.h>

TTF_Font * font = NULL;
SDL_Surface * surface = NULL;
SDL_Texture * texture = NULL;
SDL_Rect dstrect;

int highscore = 0;

int textWidth = 0;
int textHeight = 0;

void initText()
{
	font = TTF_OpenFont("fonts/font.ttf", 25);
}

void drawText(int x, int y, Uint8 r, Uint8 g, Uint8 b, int align, char *format, ...)
{
	SDL_Color color = {r, g, b};
	
	va_list args;
	
	char text[100];
	
	memset(&text, '\0', sizeof(text));
	
	va_start(args, format);
	vsprintf(text, format, args);
	va_end(args);
	
	surface = TTF_RenderText_Solid(font, (char*)&text, color);
	texture = SDL_CreateTextureFromSurface(app.renderer, surface);
	
	int texW = 0;
 	int texH = 0;
 	
	SDL_QueryTexture(texture, NULL, NULL, &texW, &texH);
	
	textWidth = texW;
	textHeight = texH;
	
	switch(align)
	{
        case TEXT_RIGHT:
            x -= textWidth;
            break;
        case TEXT_CENTER:
            x -= textWidth / 2;
            break;
	}
	
	dstrect = {x, y, texW, texH};
	
	SDL_RenderCopy(app.renderer, texture, NULL, &dstrect);
}

void drawHud()
{
	drawText(10, 10, 120, 204, 250, TEXT_LEFT, "SCORE: %03d", stage.score);
	
	//printf("SCORE: %d \n", stage.score); //TODO: Sprawdzić do czego to potrzebne, czy poprzednia linijka nie wystarczy
	
	if(stage.score > 0 && stage.score == highscore)
		drawText(1695, 10, 219, 57, 152, TEXT_LEFT, "HIGH SCORE: %03d", highscore);
		
	else
		drawText(1695, 10, 120, 204, 250, TEXT_LEFT, "HIGH SCORE: %03d", highscore); //zmiana z białego (255,255,255) na lazurowy niebieski
	
	if(pointsCounter > 1)
		drawText(10, 40, 249, 161, 188, TEXT_LEFT, "+ %d points", pointsCounter);
		
	else if(pointsCounter == 1)
		drawText(10, 40, 249, 161, 188, TEXT_LEFT, "+ %d point", pointsCounter);
		
	Uint8 r, g, b;
	
	if(bonusPoints)
	{
	    r = 219;
	    g = 57;
	    b = 152;
	}
	
	else
	{
	    r = 249;
	    g = 161;
	    b = 188;
	}
	
	Uint32 drawTextTime = SDL_GetTicks();

    Uint32 lifetime = (drawTextTime - pointCatchTime) / 1000;

    if(lifetime <= 1)
    {
        if(pointsCounter > 1)
		    drawText(10, 40, r, g, b, TEXT_LEFT, "+ %d points", pointsCounter);
		
	    else if(pointsCounter == 1)
		    drawText(10, 40, r, g, b, TEXT_LEFT, "+ %d point", pointsCounter);
    }
}

void deleteText()
{
	dstrect = {NULL, NULL, NULL, NULL};
	SDL_DestroyTexture(texture);
 	SDL_FreeSurface(surface);
}

void deinitText()
{
	deleteText();
	TTF_CloseFont(font);
}
