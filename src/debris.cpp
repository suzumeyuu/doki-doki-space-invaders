#include <debris.h>
#include <structs.h>
#include <draw.h>
#include <defs.h>
#include <cstring>
#include <cstdlib>

void doDebris()
{
	Debris *d, *prev;

	prev = &stage.debrisHead;
	
	for(d = stage.debrisHead.next; d != NULL; d = d->next)
	{
		d->x += d->dx;
		d->y += d->dy;
		
		d->dy += 0.5;
		
		if(--d->life <= 0)
		{
			if(d == stage.debrisTail)
				stage.debrisTail = prev;
				
			prev->next = d->next;
			
			delete d;
			
			d = prev;
		}
		
		prev = d;
	}
}

void addDebris(Entity *e)
{
	Debris *d = NULL;

	int w = e->w / 2;
	int h = e->h / 2;
	
	for(int y = 0; y <= h; y += h)
		for(int x = 0; x <= w; x += w)
		{
			d = new Debris;
			memset(d, 0, sizeof(Debris));
			
			stage.debrisTail->next = d;
			stage.debrisTail = d;
			
			d->x = e->x + e->w / 2;
			d->y = e->y + e->h / 2;
			d->dx = (rand() % 5) - (rand() % 5);
			d->dy = -(5 + (rand() % 12));
			d->life = FPS * 2;
			d->texture = e->texture;
			
			d->rect.x = x;
			d->rect.y = y;
			d->rect.w = w;
			d->rect.h = h;
		}
}

void drawDebris()
{
	Debris *d;
	
	for(d = stage.debrisHead.next; d != NULL; d = d->next)
		blitRect(d->texture, &d->rect, d->x, d->y);
}

