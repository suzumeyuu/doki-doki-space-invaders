#include <events.h>
#include <structs.h>
#include <defs.h>
#include <explosion.h>
#include <debris.h>
#include <util.h>
#include <sound.h>
#include <SDL.h>
#include <cstdlib>
#include <algorithm>
#include <text.h>
#include <draw.h>

SDL_Texture *bulletTexture = NULL;
SDL_Texture *enemyBulletTexture = NULL;

int pointsPods = 0;
int pointsCounter = 0;

int points = 0;

bool bonusPoints = false;

Uint32 pointCatchTime;

void doFighters()
{
	Entity *e, *prev;
	
	prev = &stage.fighterHead;
	
	for(e = stage.fighterHead.next; e != NULL; e = e->next)
	{	
		e->x += e->dx;
		e->y += e->dy;
		
		if(e != player && e->x < -e->w)
			e->health = 0;
			
		if(e->health == 0)
		{
			if(e == player)
				player = NULL;
			
			if(e == stage.fighterTail)
				stage.fighterTail = prev;
				
			prev->next = e->next;
			
			delete e;

			e = prev;
		}
		
		prev = e;
	}
}

void doBullets()
{
	Entity *b, *prev;
	
	prev = &stage.bulletHead;
	
	for(b = stage.bulletHead.next; b != NULL; b = b->next)
	{
		b->x += b->dx;
		b->y += b->dy;
		
		if(bulletHitFighter(b) || b->x < -b->w || b->y < -b->h || b->x > SCREEN_WIDTH || b->y > SCREEN_HEIGHT)
		{	
			if(b == stage.bulletTail)
				stage.bulletTail = prev;
					
			prev->next = b->next;
				
			delete b;
				
			b = prev;
		}
		
		prev = b;
	}
}

int bulletHitFighter(Entity *b)
{
	Entity *e;
	
	for(e = stage.fighterHead.next; e != NULL; e = e->next)
	{
		if(e->side != b->side && collision(b->x, b->y, b->w, b->h, e->x, e->y, e->w, e->h))
		{
			b->health = 0;
			e->health = 0;
				
            if(e == player)
				playSound(SND_GAME_OVER, CH_PLAYER);
			
			else
			{
			    pointsPods++;
			    
				addPointsPod(e->x + e->w / 2, e->y + e->h / 2);
				
				playSound(SND_ENEMY_DIE, CH_ANY);

                e->texture = enemyKilledTexture;
			}

			addExplosions(e->x, e->y, 50);
			addDebris(e);

			return 1;
		}
	}
	
	return 0;
}

void fireBullet()
{
	Entity *bullet = new Entity;
	
	memset(bullet, 0, sizeof(Entity));
	
	stage.bulletTail->next = bullet;
	stage.bulletTail = bullet;
	
	bullet->side = SIDE_PLAYER;
	bullet->x = player->x + 111;
	bullet->y = player->y - 27;
	bullet->w = 100;
	bullet->h = 100;
	bullet->dx = PLAYER_BULLET_SPEED;
	bullet->health = 1;
	bullet->texture = bulletTexture;
	
	bullet->y += (player->h / 2) - (bullet->h / 2);
	
	player->reload = 8;
}

void doPlayer()
{
	if(player != NULL)
	{
		player->dx = player->dy = 0;
	
		if(player->reload > 0)
			player->reload--;
		
		if(app.keyboard[SDL_SCANCODE_UP])
			player->dy = -PLAYER_SPEED;
	
		if(app.keyboard[SDL_SCANCODE_DOWN])
			player->dy = PLAYER_SPEED;
	
		if(app.keyboard[SDL_SCANCODE_LEFT])
			player->dx = -PLAYER_SPEED;
	
		if(app.keyboard[SDL_SCANCODE_RIGHT])
			player->dx = PLAYER_SPEED;
			
		if(app.keyboard[SDL_SCANCODE_LCTRL] && player->reload == 0)
			fireBullet();
	}
}

void doEnemies()
{
	Entity *e;
	
	for(e = stage.fighterHead.next; e != NULL; e = e->next)
		if(e != player && player != NULL && --e->reload <= 0)
			fireEnemyBullet(e);
}

void fireEnemyBullet(Entity *e)
{
	Entity *bullet = new Entity;
	
	memset(bullet, 0, sizeof(Entity));
	
	stage.bulletTail->next = bullet;
	stage.bulletTail = bullet;
	
	bullet->x = e->x;
	bullet->y = e->y;
	bullet->health = 1;
	bullet->texture = enemyBulletTexture;
	bullet->side = e->side;
	
	SDL_QueryTexture(bullet->texture, NULL, NULL, &bullet->w, &bullet->h);
	
	bullet->x += (e->w / 2) - (bullet->w / 2);
	bullet->y += (e->h / 2) - (bullet->h / 2);
	
	calcSlope(player->x + (player->w / 2), player->y + (player->h / 2), e->x, e->y, &bullet->dx, &bullet->dy);
	
	bullet->dx *= (rand() % (ENEMY_BULLET_SPEED * 2));
	bullet->dy *= (rand() % (ENEMY_BULLET_SPEED * 2));
	
	bullet->side = SIDE_ENEMY;
	
	e->reload = (rand() % FPS * 2);
}

int countPoints(int startPoints, int pointsStep, Entity *e)
{
	Uint32 currentTime = SDL_GetTicks();

    Uint32 lifetime = (currentTime - e->lifeStartTime) / 1000;

    int endPoints = startPoints - (pointsStep * lifetime);

    return endPoints;
}

void doPointsPods()
{
	Entity *e, *prev;
	
	prev = &stage.pointsHead;
	
	for(e = stage.pointsHead.next; e != NULL; e = e->next)
	{	
		if(e->x < 0)
		{
			e->x = 0;
			e->dx = -e->dx;
		}
		
		if(e->x + e->w > SCREEN_WIDTH)
		{
			e->x = SCREEN_WIDTH - e->w;
			e->dx = -e->dx;
		}
		
		if(e->y < 0)
		{
			e->y = 0;
			e->dy = -e->dy;
		}
		
		if(e->y + e->h > SCREEN_HEIGHT)
		{
			e->y = SCREEN_HEIGHT - e->h;
			e->dy = -e->dy;
		}
		
		e->x += e->dx;
		e->y += e->dy;
		
		if(e->texture == bonusPointsTexture)
            points = countPoints(25, 5, e);

        else
            points = countPoints(10, 1, e);

        if(points == 0)
            e->health = 0;
	
		else
		{
		    if(player != NULL && collision(e->x, e->y, e->w, e->h, player->x, player->y, player->w, player->h))
		    {	
		        pointCatchTime = SDL_GetTicks();
		    
			    e->health = 0;
			
			    if(e-> texture == bonusPointsTexture)
			        bonusPoints = true;
			    
			    else
			        bonusPoints = false;
			    
			    stage.score += points;
			
			    pointsCounter = points;
			
			    highscore = std::max(stage.score, highscore);
			
			    playSound(SND_POINTS, CH_POINTS);
		    }
		}
		
		if(--e->health <= 0)
		{
			if(e == stage.pointsTail)
				stage.pointsTail = prev;
			
			prev->next = e->next;
			
			delete e;
			
			e = prev;
		}
		
		prev = e;
	}
}

void addPointsPod(int x, int y)
{
	Entity *e = new Entity;
	
	memset(e, 0, sizeof(Entity));
	
	stage.pointsTail->next = e;
	stage.pointsTail = e;
	
	e->x = x;
	e->y = y;
	e->dx = -(rand() % 10);
	e->dy = (rand() % 5) - (rand() % 5);
	e->health = FPS * 10;
	
	if(pointsPods % 10 == 0)
	    e->texture = bonusPointsTexture;
	    
	else
        e->texture = pointsTexture;

	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	
	e->x -= e->w / 2;
	e->y -= e->h / 2;
	
	e->lifeStartTime = SDL_GetTicks();
}
