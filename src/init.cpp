#include <init.h>
#include <structs.h>
#include <defs.h>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <sound.h>
#include <starfield.h>
#include <text.h>
#include <stage.h>
#include <highscores.h>

void initSDL()
{
	int rendererFlags, windowFlags;

	rendererFlags = SDL_RENDERER_ACCELERATED;

	windowFlags = 0;

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("Couldn't initialize SDL: %s\n", SDL_GetError());
		exit(1);
	}
	
	app.window = SDL_CreateWindow("Doki Doki Space Invaders!", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, windowFlags);

	if (!app.window)
	{
		printf("Failed to open %d x %d window: %s\n", SCREEN_WIDTH, SCREEN_HEIGHT, SDL_GetError());
		exit(1);
	}
	
	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");

	app.renderer = SDL_CreateRenderer(app.window, -1, rendererFlags);

	if (!app.renderer)
	{
		printf("Failed to create renderer: %s\n", SDL_GetError());
		exit(1);
	}
	
    IMG_InitFlags(IMG_INIT_PNG|IMG_INIT_JPG);
    
    SDL_ShowCursor(1);

    if(Mix_OpenAudio(44100, AUDIO_S16SYS, 2, 1024) == -1)
    {
    	printf("Couldn't initialize SDL Mixer.\n");
    	exit(1);
    }
    
    Mix_AllocateChannels(MAX_SND_CHANNELS);
}

void deinitSDL()
{
    SDL_DestroyRenderer(app.renderer);
    SDL_DestroyWindow(app.window);
}

void initGame()
{
    initBackground();
    
    initStarfield();
    
    initSounds();
    
    initText();
    
    initHighscoreTable();
    
    loadMusic("music/initGame.wav"); //TODO: Dodać plik audio
}
